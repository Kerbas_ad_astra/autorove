﻿using KSP;
using UnityEngine;
using System;

namespace AutoRove
{
    internal static class autoRoveConfig
    {
        private static string configName = "autoRoveConfig";
        private static ConfigNode node;

        internal static double gravityScaleFactor
        {
            get
            {
                double gsf = Convert.ToDouble(node.GetValue("gravityScaleFactor"));
                if (gsf >= 0 && gsf <= 1)
                {
                    return gsf;
                }
                else
                {
                    autoRoveUtils.debugWarning("gravityScaleFactor not found defaulting to 1");
                    return 1;
                }
            }
        }

        internal static int framesPerUpdate
        {
            get
            {
                int fpu = fpu = Convert.ToInt16(node.GetValue("framesPerUpdate"));
                if (fpu > 0)
                {
                    return fpu;
                }
                else
                {
                    autoRoveUtils.debugWarning("framesPerUpdate not found defaulting to 60");
                    return 60;
                }
            }
        }




        internal static void load()
        {
            autoRoveUtils.debugMessage("loading AutoRove config");
            //autoRoveUtils.debugError("PluginFolder: " + GameDatabase.Instance.root.DirectoryExists("GameData/AutoRove"));
            foreach (UrlDir.UrlFile item in GameDatabase.Instance.root.AllConfigFiles)
            {
                if (item.name == configName)
                {
                    node = item.GetConfig("AUTOROVE").config;
                }
            }
            //autoRoveUtils.debugWarning("configData loaded: " + "FPU - " + framesPerUpdate + "; GSF - " + gravityScaleFactor);
        }

    }
}
